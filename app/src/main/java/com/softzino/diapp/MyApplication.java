package com.softzino.diapp;

import android.app.Application;

import com.softzino.diapp.dagger.components.AppComponent;
import com.softzino.diapp.dagger.components.DaggerAppComponent;
import com.softzino.diapp.dagger.components.DaggerPresenterComponent;
import com.softzino.diapp.dagger.components.PresenterComponent;
import com.softzino.diapp.dagger.modules.AppModule;
import com.softzino.diapp.dagger.modules.NetModule;
import com.softzino.diapp.dagger.modules.VehicleModule;

/**
 * Created by RTC on 7/4/2017.
 */

public class MyApplication extends Application {
    private static final String TAG = "MyApplication";

    AppComponent appComponent;
    PresenterComponent presenterComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .netModule(new NetModule("https://stackoverflow.com/"))
                    .appModule(new AppModule(this)).build();
        }
        return appComponent;
    }

    public PresenterComponent getPresenterComponent() {
        if (presenterComponent == null) {
            presenterComponent = DaggerPresenterComponent.builder()
                    .appComponent(getAppComponent())
                    .vehicleModule(new VehicleModule()).build();
        }
        return presenterComponent;
    }
}
