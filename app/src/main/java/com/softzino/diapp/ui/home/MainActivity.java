package com.softzino.diapp.ui.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.softzino.diapp.MyApplication;
import com.softzino.diapp.R;
import com.softzino.diapp.models.Vehicle;
import com.softzino.diapp.ui.common.DependentActivity;

import javax.inject.Inject;

import retrofit2.Retrofit;


public class MainActivity extends DependentActivity {
    @Inject
    Vehicle vehicle;
    @Inject
    Retrofit retrofit;

    private TextView textView;
    private TextView tvHelloWorld;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        createComponent();
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        tvHelloWorld = (TextView) findViewById(R.id.tvHelloWorld);
        textView = (TextView) findViewById(R.id.textView);



        if(vehicle!=null){
            tvHelloWorld.setText(" vehicle not null");
        }else{
            tvHelloWorld.setText("vehicle null");
        }

        if(retrofit!=null){
            textView.setText(" retrofit not null");
        }else{
            textView.setText("retrofit null");
        }


    }

    @Override
    protected void inject() {
        ((MyApplication)getApplication())
                .getPresenterComponent().inject(this);
    }


    private void createComponent() {

    }
}
