package com.softzino.diapp.dagger.components;

import com.softzino.diapp.dagger.annotations.ViewScope;
import com.softzino.diapp.dagger.modules.NetModule;
import com.softzino.diapp.dagger.modules.VehicleModule;
import com.softzino.diapp.ui.home.MainActivity;

import dagger.Component;

@ViewScope
@Component(modules = { VehicleModule.class }, dependencies = AppComponent.class)
public interface PresenterComponent {
    void inject(MainActivity activity);
}
