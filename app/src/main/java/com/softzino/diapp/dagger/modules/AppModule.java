package com.softzino.diapp.dagger.modules;

import com.softzino.diapp.MyApplication;
import com.softzino.diapp.dagger.annotations.AppScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RTC on 7/4/2017.
 */
@Module
public class AppModule {
    private MyApplication myApplication;

    public AppModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @AppScope
    @Provides
    public MyApplication provideApplication() {
        return myApplication;
    }
}
