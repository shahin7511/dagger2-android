package com.softzino.diapp.dagger.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RTC on 7/5/2017.
 */

@Scope
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ViewScope {
}
