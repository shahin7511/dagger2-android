package com.softzino.diapp.dagger.modules;

import com.softzino.diapp.dagger.annotations.ViewScope;
import com.softzino.diapp.models.Vehicle;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RTC on 7/4/2017.
 */
@Module
public class VehicleModule {

    @ViewScope
    @Provides
    Vehicle provideVehicle(){
        return new Vehicle();
    }
}
