package com.softzino.diapp.dagger.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Scope;
import javax.inject.Singleton;

import dagger.MapKey;

/**
 * Created by RTC on 7/5/2017.
 */
@Scope
@Retention(value = RetentionPolicy.RUNTIME)
public @interface AppScope {
}
